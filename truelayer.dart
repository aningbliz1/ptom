@override
Widget build(BuildContext context) {
  return Scaffold(
    body: WebView(
      initialUrl: 'https://staging.belyfted.com/.....',
      javascriptMode: JavascriptMode.unrestricted,
      userAgent: 'Flutter;Webview',
      navigationDelegate: (navigation) {


        //Listen for callback URL
        //payment was successful
        if (navigation.url == "https://staging.belyfted.com/payment-completed") {
          Navigator.of(context).pop(); //close webview 
          //Run the success payment function if you have one or take user to success screen
        }

        //payment failed
        if (navigation.url == "https://staging.belyfted.com/user/transfer/send-money") {
          //handle webview removal
          Navigator.of(context).pop(); //close webview
          //Run the cancel payment function if you have one or show payment failed screen
        }
        return NavigationDecision.navigate;
      },
    ),
  );
}